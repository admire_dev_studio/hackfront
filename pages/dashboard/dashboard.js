import BaseMap from '~/components/BaseMap/BaseMap.vue';

export default {
  components: {
    BaseMap,
  },
  middleware: 'isAuthorized'
}
