export default function ({ store, redirect }) {

  if(process.client) {
    if (!localStorage.token) {
      alert('Пожалуйста, пройтиде авторизацию');
      redirect('/');
      return;
    }
  }

  if (!store.user) {
    store.dispatch('checkUser');
  }
}
