const state = {
};

const actions = {
  getDevices({ commit, state }) {
    return this.$axios.$get('https://php-back.admire.social/' + 'api/obj');
  },
  getAreas({ commit, state }, coord) {
    return this.$axios.$get('https://php-back.admire.social/api/' + `area_list?latitude=${coord.lat}&longitude=${coord.lng}`);
  },
  addArea({ commit, state }, area) {
    return this.$axios.$post('https://php-back.admire.social/api/' + `add_area`, area);
  },
  getOwner({ commit, state }, body) {
    return this.$axios.$post('https://php-back.admire.social/api/' + `get_owner`, body);
  },
  getErrors({ commit, state }, body) {
    return this.$axios.$post('https://php-back.admire.social/api/' + `get_errors`, body);
  },
};

const getters = {
};

export default {
  state,
  actions,
  getters
};
