import Pusher from 'pusher-js';

const getColors = (id) => {
  return `rgba(${[
    '0, 0, 0, 0.3',
    '33, 24, 46, 0.3',
    '24, 46, 44, 0.3',
    '82, 161, 51, 0.3',
    '126, 51, 161, 0.3',
    '51, 115, 161, 0.3',
    '161, 152, 51, 0.3',
    '15, 11, 204, 0.3',
    '24, 46, 30, 0.3',
    '24, 196, 30, 0.3'
  ][id % 10]})`;
};

const state = {
  errors: [],
  location: null,
  lastSub: null
};

const actions = {
  createLine({ commit, state }, {map, point1, point2, color}) {
    const lineString = new H.geo.LineString();
    lineString.pushLatLngAlt(point1.lat, point1.lng);
    lineString.pushLatLngAlt(point2.lat, point2.lng);

    const line = new H.map.Polyline(lineString, {
      style: {
        lineWidth  : 2,
        strokeColor: color
      }
    });
    map.addObject(line);
    return line;
  },
  createArea({ commit, state }, {map, area}) {
    console.log(area);
    const lineString = new H.geo.LineString();
    lineString.pushLatLngAlt(area.left_latitude, area.left_longitude);
    lineString.pushLatLngAlt(area.right_latitude, area.left_longitude);
    lineString.pushLatLngAlt(area.right_latitude, area.right_longitude);
    lineString.pushLatLngAlt(area.left_latitude, area.right_longitude);

    const line = new H.map.Polygon(lineString, {
      style: {
        fillColor: getColors(area.user_id),
        strokeColor: 'blue',
        lineWidth: 2
      }
    });
    map.addObject(line);
    return line;
  },
  createMarker({ commit, state }, {map, lat, lng, color}) {
    const svgMarkup = '<svg width="20" x="0" y="10" height="20" viewBox="0 0 20 20" version="1.1" fill="' + color + '"' +
      '  xmlns="http://www.w3.org/2000/svg">' +
      '  <circle cx="10" cy="10" r="10"/>' +
      '</svg>';

    const icon = new H.map.Icon(svgMarkup),
      coords = {lat, lng},
      marker = new H.map.Marker(coords, {icon: icon});

    map.addObject(marker);
    return marker;
  },
  errorsSubscribe({state}, coord) {
    const sector = [
      Math.round(coord.lat - coord.lat % 2),
      Math.round(coord.lng - coord.lng % 2),
    ];

    Pusher.logToConsole = true;

    const pusher = new Pusher('8da04f0e1ecfefbeaecc', {
      cluster: 'eu',
      forceTLS: true
    });

    console.log(sector);

    if(state.lastSub !== null) state.lastSub.unsubscribe();
    state.lastSub = pusher.subscribe('error-listener');
    state.lastSub.bind(`sector-${sector[0]}-${sector[1]}`, (data) => {
      console.log(data);
      state.errors = state.errors.concat([data]);
    });
  },
  getAutocomplete({ commit, state }, text) {
    return this.$axios.$get('https://autocomplete.geocoder.ls.hereapi.com/6.2/suggest.json' +
      `?apiKey=${'jpqCEYkwL543JRJJb12V31gUT9AiwShHNSyqCTg4S4w'}` +
      `&query=${text}` +
      '&beginHighlight=<b>' +
      '&endHighlight=</b>');
  },
  getGeocoder({ commit, state }, text) {
    return this.$axios.$get('https://geocoder.ls.hereapi.com/6.2/geocode.json' +
      `?apiKey=${'jpqCEYkwL543JRJJb12V31gUT9AiwShHNSyqCTg4S4w'}` +
      `&locationid=${text}` +
      '&gen=9' +
      '&endHighlight=</b>');
  },
};

const getters = {
  sectorErrors: state => state.errors,
  lastLocation: state => state.location,
};

const mutations = {
  setLocation(state, value) {
    state.location = value;
  },
  setError(state, value) {
    state.errors = value;
  }
};

export default {
  state,
  actions,
  getters,
  mutations
};
