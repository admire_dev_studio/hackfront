import Vue from 'vue';
import Vuex from 'vuex';
import app from './app.module';
import device from './device.module';
import auth from './auth.module';

Vue.use(Vuex);

const store = () => new Vuex.Store({
  modules: {
    device,
    app,
    auth,
  },
});

export default store;
