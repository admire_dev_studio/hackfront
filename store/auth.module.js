const state = {
  user: null
};

const actions = {
  checkUser({ commit, state, router }) {
    this.$axios.$get('https://php-back.admire.social/api/' + 'user').then(data => {
      state.user = data.user;
    });
  },
  login({ commit, state }, data) {
    this.$axios.$post('https://php-back.admire.social/api/' + 'sign_in', data).then(data => {
      state.user = data.user;
      localStorage.token = data.token;
      this.$axios.setHeader('Authorization', data.token);
      this.$router.push('/dashboard');
    });
  },
  register({ commit, state }, data) {
    this.$axios.$post('https://php-back.admire.social/api/' + 'sign_up', data).then(data => {
      state.user = data.user;
      localStorage.token = data.token;
      this.$axios.setHeader('Authorization', data.token);
      this.$router.push('/dashboard');
    });
  },
  getUsers({ commit, state }) {
    return this.$axios.$get('https://php-back.admire.social/api/' + 'user_list');
  }
};

const getters = {
};

export default {
  state,
  actions,
  getters
};
