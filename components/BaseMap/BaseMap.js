import { mapActions, mapGetters, mapMutations, mapState } from 'vuex';

import NodeController from "../NodeController";

export default {
  name: "BaseMap",

  components: {NodeController},

  data() {
    return {
      map: null,
      platform: {},
      nodesGroup: {},
      areasGroup: {},
      dialog: false,
      isOpenController: false,
      updateTimeout: 0,
      openedDevice: {},
      openedMarker: {},
      isNodesOrAreas: true,
      userToAddArea: null,
      areaPoints: [],
      areaAddGroup: {},
      ui: {},
      nodeOwner: null
    };
  },


  mounted() {
    this.platform = new H.service.Platform({
      apikey: 'jpqCEYkwL543JRJJb12V31gUT9AiwShHNSyqCTg4S4w'
    });

    const pixelRatio    = window.devicePixelRatio || 1;
    const defaultLayers = this.platform.createDefaultLayers({
      lg      : 'rus',
      tileSize: pixelRatio === 1 ? 256      : 512,
      ppi     : pixelRatio === 1 ? undefined: 320
    });

    const startCenter = this.lastLocation ?
      { lat: this.lastLocation.Latitude, lng: this.lastLocation.Longitude } :
      localStorage.last_map_position ? JSON.parse(localStorage.last_map_position) :
        {"lat":44.721338862511864,"lng":37.772071140893296};

    this.$store.dispatch('errorsSubscribe', startCenter);

    this.map = new H.Map(
      document.getElementById('dashboardMap'),
      defaultLayers.vector.normal.map,
      {
        zoom: 10,
        center: startCenter
      });

    this.setLocation(null);

    window.addEventListener('resize', this.resizeMap);

    this.ui = H.ui.UI.createDefault(this.map, defaultLayers, 'ru-RU');

    const behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(this.map));

    this.nodesGroup = new H.map.Group();
    this.map.addObject(this.nodesGroup);
    this.areasGroup = new H.map.Group();
    this.areasGroup.setVisibility(false);
    this.map.addObject(this.areasGroup);
    this.areaAddGroup = new H.map.Group();
    this.areaAddGroup.setVisibility(false);
    this.map.addObject(this.areaAddGroup);

    this.map.addEventListener('tap', (evt) => {
      const coord = this.map.screenToGeo(evt.currentPointer.viewportX,
        evt.currentPointer.viewportY);

      console.log(coord);

      if (this.userToAddArea) {
        this.areaAddGroup.setVisibility(true);
        this.areaPoints.push(coord);
        if (this.areaPoints.length > 2) {
          this.areaPoints.splice(0, 1);
        }
        if (this.areaPoints.length === 2) {
          this.areaAddGroup.removeAll();
          this.createArea(
            {
              map: this.areaAddGroup, area: {
              user_id: this.userToAddArea.id,
              left_latitude: Math.min(this.areaPoints[0].lat, this.areaPoints[1].lat),
              left_longitude: Math.min(this.areaPoints[0].lng, this.areaPoints[1].lng),
              right_latitude: Math.max(this.areaPoints[0].lat, this.areaPoints[1].lat),
              right_longitude: Math.max(this.areaPoints[0].lng, this.areaPoints[1].lng),
            }
          });
        }
      }
    });

    this.getAreasUpdate(startCenter);

    this.nodesGroup.addEventListener('tap', (event) => {
      this.dialog = true;
      this.openedDevice = event.target.getData();
      console.log(this.openedDevice);
      this.openedMarker = event.target;
      this.getOwner({latitude: event.target.b.lat, longitude: event.target.b.lng}).then(data => {
        this.nodeOwner = data;
      });
    });

    this.map.addEventListener('mapviewchange', this.updateMap);

    this.getDevices().then((data) => {
      this.addMarkers(data);
    });


    if (localStorage.user_to_add_area) {
      this.userToAddArea = JSON.parse(localStorage.user_to_add_area);
      localStorage.removeItem('user_to_add_area');
      this.nodesGroup.setVisibility(false);
      this.areasGroup.setVisibility(false);
    }
  },

  created() {
    this.$store.watch(
      (state, getters) => getters.lastLocation,
      (val) => {
        if (!val) return;
        if (this.map) {
          this.map.setCenter({ lat: val.Latitude, lng: val.Longitude });
          this.setLocation(null);
        }
      },
    );
  },

  watch: {
    isNodesOrAreas(val) {
      this.nodesGroup.setVisibility(val);
      this.areasGroup.setVisibility(!val);
      this.areaAddGroup.removeAll();
      this.areaPoints = [];
    },
    sectorErrors(val) {
      for (let i = 0; i < val.length; i++) {
        this.nodesGroup.getObjects().forEach(async (marker) => {
          if (marker.getData() && marker.getData()._id && marker.getData().state.upr) {
            val.forEach(error => {
              if (error.node_id === marker.getData()._id && error.hran_result === 'bad') {
                const svgMarkup = '<svg width="20" x="0" y="10" height="20" viewBox="0 0 20 20" version="1.1" fill="orange"' +
                  '  xmlns="http://www.w3.org/2000/svg">' +
                  '  <circle cx="10" cy="10" r="10"/>' +
                  '</svg>';

                const icon = new H.map.Icon(svgMarkup);
                marker.setIcon(icon);
              }
            });
            setTimeout(() => {
              val.forEach(error => {
                if (error.node_id === marker.getData()._id && error.hran_result === 'bad') {
                  const svgMarkup = '<svg width="20" x="0" y="10" height="20" viewBox="0 0 20 20" version="1.1" fill="blue"' +
                    '  xmlns="http://www.w3.org/2000/svg">' +
                    '  <circle cx="10" cy="10" r="10"/>' +
                    '</svg>';

                  const icon = new H.map.Icon(svgMarkup);
                  marker.setIcon(icon);
                }
              });
            }, 1000 * 50);
          }
        });
      }
      this.setError([]);
    }
  },

  destroyed() {
    window.removeEventListener('resize', this.resizeMap);
  },

  methods: {
    getAreasUpdate(coour) {
      this.getAreas(coour).then((data) => {
        data.forEach(area => {
          this.createArea({map: this.areasGroup, area}).then(marker => {
            const infoBubble = new H.ui.InfoBubble({
              lat: area.right_latitude,
              lng: area.right_longitude
            }, {
              content: `Ответственный на зону:<br />${area.name} - ${area.email}`
            });

            marker.addEventListener('pointerenter', () => {
              this.ui.addBubble(infoBubble);
              infoBubble.open();
            }, false);

            marker.addEventListener('pointerleave', () => {
              infoBubble.close();
            }, false);
          });
        });
      });
    },
    resizeMap() {
      this.map.getViewPort().resize()
    },
    updateMap() {
      clearTimeout(this.updateTimeout);
      this.updateTimeout = setTimeout(() => {
        localStorage.setItem('last_map_position', JSON.stringify(this.map.getCenter()));
        this.updateErrors();

        this.getAreasUpdate(this.map.getCenter());

        this.$store.dispatch('errorsSubscribe', this.map.getCenter());
      }, 2000);
    },
    updateErrors() {
      this.getErrors({latitude: this.map.getCenter().lat, longitude: this.map.getCenter().lng}).then(async (data) => {
        this.nodesGroup.getObjects().forEach(marker => {
          if (marker.getData() && marker.getData()._id && marker.getData().state.upr) {
            data.forEach(error => {
              if (error.node_id === marker.getData()._id && error.hran_result === 'bad') {
                const svgMarkup = '<svg width="20" x="0" y="10" height="20" viewBox="0 0 20 20" version="1.1" fill="orange"' +
                  '  xmlns="http://www.w3.org/2000/svg">' +
                  '  <circle cx="10" cy="10" r="10"/>' +
                  '</svg>';

                const icon = new H.map.Icon(svgMarkup);
                marker.setIcon(icon);
              }
            });
            setTimeout(() => {
              data.forEach(error => {
                if (error.node_id === marker.getData()._id && error.hran_result === 'bad') {
                  const svgMarkup = '<svg width="20" x="0" y="10" height="20" viewBox="0 0 20 20" version="1.1" fill="blue"' +
                    '  xmlns="http://www.w3.org/2000/svg">' +
                    '  <circle cx="10" cy="10" r="10"/>' +
                    '</svg>';

                  const icon = new H.map.Icon(svgMarkup);
                  marker.setIcon(icon);
                }
              });
            }, 1000 * 50);
          }
        });
      });
    },
    cancelAddArea() {
      this.userToAddArea = null;
      this.nodesGroup.setVisibility(true);
      this.areasGroup.setVisibility(false);
      localStorage.removeItem('user_to_add_area');
    },
    addAreaClick() {
      let area = {
        user_id: this.userToAddArea.id,
        left_latitude: Math.min(this.areaPoints[0].lat, this.areaPoints[1].lat),
        left_longitude: Math.min(this.areaPoints[0].lng, this.areaPoints[1].lng),
        right_latitude: Math.max(this.areaPoints[0].lat, this.areaPoints[1].lat),
        right_longitude: Math.max(this.areaPoints[0].lng, this.areaPoints[1].lng),
      };
      this.addArea(area);
      area = Object.assign({}, area, this.userToAddArea);
      this.createArea({map: this.areasGroup, area}).then(marker => {
          const infoBubble = new H.ui.InfoBubble({
            lat: area.right_latitude,
            lng: area.right_longitude
          }, {
            content: `Ответственный на зону:<br />${area.name} - ${area.email}`
          });

          marker.addEventListener('pointerenter', function() {
            this.ui.addBubble(infoBubble);
            infoBubble.open();
          }, false);

          marker.addEventListener('pointerleave', function() {
            infoBubble.close();
          }, false);
        });
      this.isNodesOrAreas = false;
      localStorage.removeItem('user_to_add_area');
    },
    ...mapActions([
      'createLine',
      'createMarker',
      'getDevices',
      'createArea',
      'getAreas',
      'addArea',
      'getOwner',
      'getErrors'
    ]),
    ...mapMutations([
      'setLocation',
      'setError'
    ]),
    addMarkers(data) {
      this.updateErrorsMarkers(data);
    },
    req(marker, concat_id) {

      let line = null;
      let m2 = null;
      m2 = this.nodesGroup.getObjects().find(m => m.getData()._id === concat_id);
      if (m2) {
        let nexts = [];

        if (m2.getData().name.startsWith('Trub') && (m2.getData().name.endsWith('0') || m2.getData().name.endsWith('1'))
          && m2.getData().state.dat_truba) nexts = m2.getData().state.dat_truba;
        else if (m2.getData().name.startsWith('Trub') && m2.getData().state.dat_house) nexts = m2.getData().state.dat_house;
        if (m2.getData().name.startsWith('Vent_Name') && m2.getData().state.dat_truba) nexts = m2.getData().state.dat_truba;

        line = this.createLine({
          map: this.nodesGroup,
          point1: marker.b,
          point2: m2.b,
          color: marker.getData().state.upr || marker.getData().name.startsWith('NiberVodohr') ? 'blue' : 'gray'
        });
        if (m2.getData().state.upr === true)
          m2.getData().state.upr = marker.getData().state.upr || marker.getData().name.startsWith('NiberVodohr');
        if (!m2.getData().state.upr) {
          let svgMarkup = '<svg width="20" x="0" y="10" height="20" viewBox="0 0 20 20" version="1.1" fill="gray"' +
            '  xmlns="http://www.w3.org/2000/svg">' +
            '  <circle cx="10" cy="10" r="10"/>' +
            '</svg>';

          if(marker.getData().state.upr || marker.getData().name.startsWith('NiberVodohr')) {
            svgMarkup = '<svg width="20" x="0" y="10" height="20" viewBox="0 0 20 20" version="1.1" fill="red"' +
              '  xmlns="http://www.w3.org/2000/svg">' +
              '  <circle cx="10" cy="10" r="10"/>' +
              '</svg>';
          }

          const icon = new H.map.Icon(svgMarkup);
          m2.setIcon(icon);
        }
        if (line && !marker.getData().name.startsWith('NiberVodohr')) {
          line.then(l => l.data = {
            type: 'line',
            start: marker.getData(),
            end: m2.getData(),
          });
        }

        nexts.forEach(next => {
          this.req(m2, next);
        });
      }
    },
    async updateErrorsMarkers(data) {
      for (const val of data) {
        const marker = await this.createMarker({map: this.nodesGroup, lat: val.state.lat, lng: val.state.long, color:
            val.name.startsWith('NiberVodohr') || val.state.upr !== false ? 'blue' : 'red'
        });
        marker.setData(val);
      }
      const trubas = [];

      this.nodesGroup.getObjects()[0].getData().state.ventils.forEach(n => {
        this.req(this.nodesGroup.getObjects()[0], n);
      });
    }
  },

  computed: {
    ...mapGetters([
      'sectorErrors',
      'lastLocation'
    ])
  },
}
